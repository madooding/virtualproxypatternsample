
public class HighResolutionImage implements Image {
	
	public HighResolutionImage(String imageFilePath) {
		// TODO Auto-generated constructor stub
		loadImage(imageFilePath);
		
	}
	
	public void loadImage(String imageFilePath){
		System.out.println("Image is loadding from " + imageFilePath);
	}
	
	@Override
	public void showImage() {
		System.out.println("Image displayed on the screen.");
	}

}
