
public class ImageProxy implements Image {
	
	private String imageFilePath;
	
	private Image proxyFileImage;
	
	public ImageProxy(String imageFilePath){
		this.imageFilePath = imageFilePath;
	}
	
	@Override
	public void showImage() {
		proxyFileImage = new HighResolutionImage(imageFilePath);
		proxyFileImage.showImage();
	}

}
