
public class VirtualProxyPatternSample {
	public static void main(String[] args){
		ImageProxy image = new ImageProxy("/img/image.jpg");
		image.showImage();
	}
}
